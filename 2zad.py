def main():
    
    ulice=[]
    
    while True:
        ulica = input("Unesi ime ulice: ")
        if ulica == "prekid":
            break
        
        elif len(ulica) <= 7 or len(ulica) >= 15:
            print("Ime mora biti izmedu 7 i 15 znakova")
            continue
        
        ulice.append(ulica)
        
    print("Uneseno je %d ulica"%(len(ulice)))
    najduza = ""
    for ulica in ulice:
        if len(ulica)>len(najduza):
            najduza=ulica
            
    print("Najduza ulica je: %s"%(najduza))
main()
