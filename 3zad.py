studenti={}
while True:
    ime = input("Unesi ime: ")
    if ime == "":
        break
    ocjena = int(input("Unesi ocjenu: "))
    studenti[ime]= ocjena

print(studenti)
print (studenti.values())
prosjek = sum(studenti.values())/len(studenti.values())
print("Projek je %.2f"%(prosjek))

maxocjena = 0
maxstudent = ""
for ime in studenti:
    if studenti[ime] > maxocjena:
        maxocjena = studenti[ime]
        maxstudent = ime

print ("Student s max ocjenom je %s"%(maxstudent))
