import random

number = random.randint(0,15)
guess = 0
br = 0

while guess != number and guess != "Bye":
    
    guess = input("Pogađaj broj od 1 do 15: ")
    
    if guess == "Bye":
        break

    guess = int(guess)
    br += 1


    if guess == number:
        print("Uspio si, broj pokušaja", br)
